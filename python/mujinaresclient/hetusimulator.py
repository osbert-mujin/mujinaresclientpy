# -*- coding: utf-8 -*-

import typing
import threading
import time
import queue
from mujinplc import PLCDataObject

from .aresmessage import Message, MessageType, COMMAND_TYPE_MAPPING
from .aresserversocket import ServerSocket

import logging

log = logging.getLogger(__name__)

try:
    from mujincontrollerclient.realtimerobotclient import RealtimeRobotControllerClient
    from mujincontrollerclient.controllerclientraw import ControllerWebClient
    from mujincontrollerclient import controllerclientbase
except ImportError as e:
    log.info("mujincontrollerclientpy not installed. you aren't able to use visionsimulation function")


class Task(PLCDataObject):
    """
    Ares Task info . This is used to communicate between arec client and ares rcs.
    """
    actionID = 0  # type: int
    armID = 0  # type: int
    partType = ''  # type: str
    orderNumber = 0  # type: int

    # pick location info
    pickLocationReady = 0  # type: int # 0 is ready , 1 is not ready .
    pickLocationIndex = 0  # type: int
    pickContainerId = ''  # type: str
    pickContainerType = ''  # type: str

    # place location info
    placeLocationReady = 0  # type: int # 0 is ready , 1 is not ready .
    placeLocationIndex = 0  # type: int
    placeContainerId = ''  # type: str
    placeContainerType = ''  # type: str

    # packing
    packInputPartIndex = 0  # type: int
    packFormationComputationName = ''  # type: str


class HetuSimulator(object):
    _socket = None  # type: ServerSocket

    _listenerThread = None  # type: typing.Optional[threading.Thread]
    _queueTaskThread = None  # type: typing.Optional[threading.Thread]
    _visionsimulationthread = None

    _taskQueue = None  # type: queue.Queue[Task] # remaining tasks yet to be sent to aresclient
    _isok = False  # type: bool
    _timeout = 1.0  # type: float

    _warehouse = '1'  # type: str
    _deviceType = 1  # type: int # for arm should always be 1
    _rid = 1  # type: int

    _config = {}  # user input configuration
    _registered = 0  # type: float

    def __init__(self, mujinAresClientEndpoint: typing.Tuple[str, int],
                 timeout: float = 10.0, **kwargs):
        self._socket = ServerSocket(mujinAresClientEndpoint)
        self._timeout = timeout
        self._warehouse = kwargs.get('warehouseID', '')
        self._armID = int(kwargs.get('armID', 0))
        self._deviceType = kwargs.get('deviceType', 1)
        self._config = kwargs.copy()
        self._rid = self._config.get('armID', 1)
        self._taskQueue = queue.Queue()

    def Start(self):
        self.Stop()

        self._socket.Start()

        self._isok = True
        self._listenerThread = threading.Thread(target=self._RunListenerThread, name='listenerthread')
        self._listenerThread.start()

        self._queueTaskThread = threading.Thread(target=self._RunQueueTaskThread, name='queuetaskthread')
        self._queueTaskThread.start()

    def Stop(self):
        self._isok = False
        self._socket.Stop()

        if self._listenerThread is not None:
            self._listenerThread.join()
            self._listenerThread = None

        if self._queueTaskThread is not None:
            self._queueTaskThread.join()
            self._queueTaskThread = None

    def _RunListenerThread(self):

        while self._isok:
            # deal with requests from aresclient
            connection, request = self._socket.ReceiveRequest(timeout=0.1)
            if not request:
                continue
            messageType = request.body["type"]
            if messageType in COMMAND_TYPE_MAPPING:
                if messageType == 'resourceRelease':
                    log.warning("retrieve resourceRelease request")
                    log.warning(request.body)
                time.sleep(1)
                self._socket.SendResponse(connection, request, Message({
                    'type': request.body['type'],
                    'warehouseID': self._warehouse,
                    'deviceType': self._deviceType,
                    'params': {
                        "errorCode": 0,
                        "seq": int(time.time() * 1000)
                    }
                }))
            else:
                pass

    def _RunQueueTaskThread(self):
        log.info('Queue Task thread started')
        while self._isok:
            # Any tasks available? If yes, send to aresclient
            while True:
                task = self._taskQueue.get()
                if task is not None:
                    # TODO: handle response
                    _ = self._socket.SendRequest(Message({
                        "warehouseID": self._warehouse,
                        "deviceType": self._deviceType,
                        "type": "move",
                        "params": {
                            "actionID": task.actionID,
                            "armID": task.armID,
                            "partType": task.partType,
                            "orderNumber": task.orderNumber,
                            "pickLocationReady": task.pickLocationReady,
                            "pickLocationIndex": task.pickLocationIndex,
                            "pickContainerId": task.pickContainerId,
                            "pickContainerType": task.pickContainerType,
                            "placeLocationReady": task.placeLocationReady,
                            "placeLocationIndex": task.placeLocationIndex,
                            "placeContainerId": task.placeContainerId,
                            "placeContainerType": task.placeContainerType,
                            "packInputPartIndex": task.packInputPartIndex,
                            "packFormationComputationName": task.packFormationComputationName
                        }
                    }), retry=0, timeout=self._timeout, messageType=MessageType.RPC)
                else:
                    break
            time.sleep(0.1)

    def QueueTask(self, task: Task) -> None:
        self._taskQueue.put(task)
        log.info('QueueTask finished ==============================')
