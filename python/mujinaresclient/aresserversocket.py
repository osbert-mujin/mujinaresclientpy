# -*- coding: utf-8 -*-

import eventfd
import struct
import time
import socket
import select
import threading
import typing

from .aresmessage import Message, MessageType

import logging

log = logging.getLogger(__name__)


class ConnectionMessagePair(object):
    connection = None  # type: socket.socket
    message = None  # type: Message

    def __init__(self, connection: socket.socket, message: Message):
        self.connection = connection
        self.message = message


class ServerSocket:
    _endpoint = None  # type: typing.Tuple[str, int]
    _thread = None  # type: typing.Optional[threading.Thread]
    _isok = False  # type: bool
    _connected = 0  # type: float # timestamp of when socket last connected
    _sequence = 0  # type: int # request sequence number

    _lock = None  # type: threading.Lock
    _receiveCondition = None  # type: threading.Condition
    _receiveQueue = None  # type: typing.List[ConnectionMessagePair]
    _receiveQueueCapcity = 128  # type: int
    _sendEvent = None  # type: eventfd.EventFD
    _sendQueue = None  # type: typing.List[ConnectionMessagePair]
    _sendQueueCapcity = 128  # type: int

    _connections = None  # type: typing.List[socket.socket]

    def __init__(self, endpoint: typing.Tuple[str, int]):
        self._endpoint = endpoint
        self._lock = threading.Lock()
        self._receiveCondition = threading.Condition(self._lock)
        self._receiveQueue = []
        self._sendEvent = eventfd.EventFD()
        self._sendQueue = []
        self._connections = []

    def __del__(self):
        self.Stop()

    def Start(self) -> None:
        self.Stop()

        self._isok = True
        self._thread = threading.Thread(target=self._RunThread, name='aressocket')
        self._thread.start()

    def SetStop(self) -> None:
        self._isok = False

    def Stop(self) -> None:
        self.SetStop()
        if self._thread:
            self._thread.join()
            self._thread = None

    def GetConnectedTimestamp(self) -> float:
        return self._connected

    def SendRequest(self, message: Message, retry: int = 0, timeout: float = 2.0,
                    messageType: MessageType = MessageType.RPC) -> typing.Optional[Message]:
        trial = 0
        starttime = time.monotonic()
        while trial <= retry:
            trial += 1
            with self._lock:
                # hack
                while len(self._connections) < 1:
                    time.sleep(0.1)
                connection = self._connections[0]
                message.type = messageType
                # message.seq = self._sequence
                if message.body:
                    message.body['msgMode'] = int(messageType)
                    message.body['msgSeq'] = self._sequence
                self._sequence += 1
                self._sequence = self._sequence  # TODO: seq in head maximum is 0<=seq<=255
                self._sendQueue.append(ConnectionMessagePair(connection=connection, message=message))
                if len(self._sendQueue) > self._sendQueueCapcity:
                    self._sendQueue = self._sendQueue[-self._sendQueueCapcity + message.body['msgSeq']]
                self._sendEvent.set()
                if messageType == MessageType.RPC:
                    while time.monotonic() - starttime < timeout:
                        self._receiveCondition.wait(0.1)
                        for response in self._receiveQueue:
                            # type and seq in header isn't used, use msgMode and msgSeq in body instead
                            # we extract type and seq from body into header when we receive the message
                            if response.message.type == MessageType.ACK and response.message.body['msgSeq'] == \
                                    message.body['msgSeq']:
                                self._receiveQueue.remove(response)
                                return response.message
        return None

    def ReceiveRequest(self, timeout: float = 2.0) -> typing.Tuple[
            typing.Optional[socket.socket], typing.Optional[Message]]:
        starttime = time.monotonic()
        with self._lock:
            while time.monotonic() - starttime < timeout:
                self._receiveCondition.wait(0.1)
                for connectionRequestPair in self._receiveQueue:
                    if connectionRequestPair.message.type == MessageType.RPC:
                        self._receiveQueue.remove(connectionRequestPair)
                        return connectionRequestPair.connection, connectionRequestPair.message
        return None, None

    def SendResponse(self, connection: socket.socket, request: Message, response: Message) -> None:
        with self._lock:
            response.type = MessageType.ACK
            # TODO: msgSeq in body is a long type,
            # which is wrong if we just copy and pack it into head response, set to 0 for now.

            # response.seq = request.seq
            response.seq = 0
            if response.body:
                response.body['msgMode'] = int(MessageType.ACK)
                response.body['msgSeq'] = request.seq
            self._sendQueue.append(ConnectionMessagePair(connection=connection, message=response))
            if len(self._sendQueue) > self._sendQueueCapcity:
                self._sendQueue = self._sendQueue[-self._sendQueueCapcity:]
            self._sendEvent.set()

    def _RunThread(self) -> None:
        serverSocket = None  # type: typing.Optional[socket.socket]
        data = b''  # type: bytes # buffer

        while self._isok:
            if serverSocket is None:
                log.debug("attempt to create new socket")
                newSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                try:
                    newSocket.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack('ii', 0, 0))
                    newSocket.bind(self._endpoint)
                    newSocket.listen(1)
                except Exception as e:
                    log.exception('error binds to %s: %s', self._endpoint, e)
                    time.sleep(0.2)
                    continue
                serverSocket = newSocket
                self._connected = time.monotonic()
                log.debug('socket is listening at %s', self._endpoint)

            inputs = [serverSocket, self._sendEvent] + self._connections

            rlist, wlist, xlist = select.select(inputs, [], [serverSocket], 0.2)

            # something to receive
            for _sock in rlist:
                if _sock == serverSocket:
                    clientConn, _ = serverSocket.accept()
                    clientConn.setblocking(False)
                    # Add to inputs
                    self._connections.append(clientConn)
                elif _sock == self._sendEvent:
                    with self._lock:
                        messages = self._sendQueue
                        self._sendQueue = []
                    for message in messages:
                        log.debug('sending message: %r', message.message)
                        try:
                            message.connection.sendall(message.message.Serialize())
                        except Exception as e:
                            log.error('socket send error, message = %s, recreating, error= %s' % (message.message, e))
                            continue
                else:
                    # Then this is from client connection, get ready to receive data:
                    try:
                        data += _sock.recv(4096)
                    except Exception as e:
                        log.error('socket receive error, closing socket: %s', e)
                        self._connections.remove(_sock)
                        continue
                    received = False
                    while True:
                        if data != b'':
                            log.debug('received data %s' % data)
                        message, data = Message.Deserialize(data)
                        if message:
                            log.debug('received message: %r', message)
                            received = True
                            with self._lock:
                                self._receiveQueue.append(ConnectionMessagePair(connection=_sock, message=message))
                                if len(self._receiveQueue) > self._receiveQueueCapcity:
                                    self._receiveQueue = self._receiveQueue[-self._receiveQueueCapcity:]
                        else:
                            break
                    if received:
                        with self._receiveCondition:
                            self._receiveCondition.notify_all()

        if serverSocket is not None:
            try:
                serverSocket.close()
            except Exception as e:
                log.exception('close socket error: %s', e)
        self._connected = 0
