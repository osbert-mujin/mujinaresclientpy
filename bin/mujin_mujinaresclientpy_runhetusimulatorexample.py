#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import sys
import time
import argparse
import json
import asyncio
from mujinaresclient import hetusimulator

log = logging.getLogger(__name__)


def _ConfigureLogging(logLevel=logging.DEBUG, outputStream=sys.stderr):
    handler = logging.StreamHandler(outputStream)
    try:
        # noinspection PyUnresolvedReferences,PyPackageRequirements
        import logutils.colorize
        handler = logutils.colorize.ColorizingStreamHandler(outputStream)
        handler.level_map[logging.DEBUG] = (None, 'green', False)
        handler.level_map[logging.INFO] = (None, None, False)
        handler.level_map[logging.WARNING] = (None, 'yellow', False)
        handler.level_map[logging.ERROR] = (None, 'red', False)
        handler.level_map[logging.CRITICAL] = ('white', 'magenta', True)
    except ImportError:
        pass
    handler.setFormatter(
        logging.Formatter('%(asctime)s %(name)s [%(levelname)s] [%(filename)s:%(lineno)s %(funcName)s] %(message)s'))
    handler.setLevel(logLevel)

    root = logging.getLogger()
    root.setLevel(logLevel)
    root.handlers = []
    root.addHandler(handler)


async def RunAsync(client: hetusimulator.HetuSimulator):
    await asyncio.sleep(5)
    task1 = hetusimulator.Task(
                            actionID=21,
                            armID=1,
                            partType="cex_oreo",
                            orderNumber=1,
                            pickLocationReady=1,
                            pickLocationIndex=1,
                            pickContainerId="1",
                            pickContainerType="",
                            placeLocationReady=1,
                            placeLocationIndex=4,
                            placeContainerId="4",
                            placeContainerType="",
                            packInputPartIndex=0,
                            packFormationComputationName=""
                        )
    client.QueueTask(task1)
    await asyncio.sleep(5)
    task2 = hetusimulator.Task(
                            actionID=21,
                            armID=1,
                            partType="cex_oreo",
                            orderNumber=1,
                            pickLocationReady=0,
                            pickLocationIndex=1,
                            pickContainerId="1",
                            pickContainerType="",
                            placeLocationReady=0,
                            placeLocationIndex=4,
                            placeContainerId="4",
                            placeContainerType="",
                            packInputPartIndex=0,
                            packFormationComputationName=""
                        )
    client.QueueTask(task2)


if __name__ == '__main__':
    _ConfigureLogging()

    parser = argparse.ArgumentParser(description='Run hetu simulator.')
    parser.add_argument('--config', action='store', type=str, dest='configfilename', required=True)
    options = parser.parse_args()

    """
    Config file example
    {
        "serverip": "192.168.1.246",
        "port": 40011,
        "armID": 1,
        "warehouseID": "227375617572601865",
        "deviceType": 1
    }
    """
    config = {}
    with open(options.configfilename, 'r') as f:
        try:
            config = json.loads(f.read())
        except Exception as e:
            log.error('read config file %s error = %s' % (options.configfilename, e))
            sys.exit(1)

    configkeyset = {'serverip', 'port', 'armID', 'warehouseID', 'deviceType'}
    if configkeyset != set(config.keys()) & configkeyset:
        log.error('config should include %s' % (configkeyset - set(config.keys())))
        sys.exit(1)

    simulator = hetusimulator.HetuSimulator((config['serverip'], int(config['port'])), **config)
    simulator.Start()

    # TODO(simon): handle signals
    loop = asyncio.get_event_loop()
    loop.run_until_complete(RunAsync(simulator))
    time.sleep(10000)

    simulator.Stop()
