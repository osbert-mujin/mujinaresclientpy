# -*- coding: utf-8 -*-
# Copyright (C) 2012-2014 MUJIN Inc
from distutils.core import setup
from distutils.dist import Distribution

setup(
    distclass=Distribution,
    name='MujinAresClient',
    version='0.1.0',
    packages=['mujinaresclient'],
    package_dir={'mujinaresclient': 'python/mujinaresclient'},
    scripts=['bin/mujin_mujinaresclientpy_runexample.py'],
    license='Apache License, Version 2.0',
    long_description=open('README.md').read(),
)
